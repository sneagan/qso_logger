extern crate chrono;
#[macro_use]
extern crate diesel;
extern crate dotenv;

use chrono::prelude::*;
use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;
use std::io::{stdin, Read};
mod crud;
mod models;
mod schema;
use models::{LogEntry, NewLogEntry};

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

fn main() {
    use schema::entries::dsl::{entries};
    let connection = establish_connection();

    println!("What was name?");
    let mut name = String::new();
    stdin().read_line(&mut name).unwrap();
    let name = &name[..(name.len() - 1)];

    println!("What was callsign?");
    let mut callsign = String::new();
    stdin().read_line(&mut callsign).unwrap();
    let callsign = &callsign[..(callsign.len() - 1)];

    let start_time: NaiveDateTime = Local::now().naive_local();
    let end_time: NaiveDateTime = Local::now().naive_local();

    println!("What was frequency?");
    let mut frequency = String::new();
    stdin().read_line(&mut frequency).unwrap();
    let frequency = &frequency[..(frequency.len() - 1)].parse::<i32>().unwrap();

    println!("What was mode?");
    let mut mode = String::new();
    stdin().read_line(&mut mode).unwrap();
    let mode = &mode[..(mode.len() - 1)];

    println!("What was qth?");
    let mut qth = String::new();
    stdin().read_line(&mut qth).unwrap();
    let qth = &qth[..(qth.len() - 1)];

    println!("What was state?");
    let mut state = String::new();
    stdin().read_line(&mut state).unwrap();
    let state = &state[..(state.len() - 1)];

    println!("What was country?");
    let mut country = String::new();
    stdin().read_line(&mut country).unwrap();
    let country = &country[..(country.len() - 1)];

    println!("What was grid?");
    let mut grid = String::new();
    stdin().read_line(&mut grid).unwrap();
    let grid = &grid[..(grid.len() - 1)];

    println!("What was power?");
    let mut power = String::new();
    stdin().read_line(&mut power).unwrap();
    let power = &power[..(power.len() - 1)].parse::<i32>().unwrap();

    println!("What was notes?");
    let mut notes = String::new();
    stdin().read_line(&mut notes).unwrap();
    let notes = &notes[..(notes.len() - 1)];

    let entry = crud::create_entry(
        &connection, 
        callsign,
        start_time,
        end_time,
        frequency,
        mode,
        name,
        qth,
        state,
        country,
        grid,
        power,
        notes,
    );
    println!("\nSaved entry {} with id {}",
        callsign,
        entry.id,
    );

    let results = entries.limit(5)
        .load::<LogEntry>(&connection)
        .expect("Error loading entries");

    println!("Displaying {}", results.len());
    for entry in results {
        println!("{}", entry.name);
        println!("--------\n");
        println!("{}", entry.callsign);
    }
}

#[cfg(not(windows))]
const EOF: &'static str = "CTRL+D";

#[cfg(windows)]
const EOF: &'static str = "CTRL+Z";
