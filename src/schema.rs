table! {
    entries (id) {
        id -> Int4,
        callsign -> Varchar,
        start_time -> Timestamp,
        end_time -> Timestamp,
        frequency -> Int4,
        mode -> Varchar,
        name -> Text,
        qth -> Text,
        state -> Text,
        country -> Text,
        grid -> Text,
        power -> Int4,
        notes -> Text,
    }
}
