use models::{LogEntry, NewLogEntry};
use chrono::prelude::*;
use diesel::*;
use diesel::pg::PgConnection;

pub fn create_entry<'a>(
    conn: &PgConnection, 
    callsign: &'a str,
    start_time: NaiveDateTime,
    end_time: NaiveDateTime,
    frequency: &'a i32,
    mode: &'a str,
    name: &'a str,
    qth: &'a str,
    state: &'a str,
    country: &'a str,
    grid: &'a str,
    power: &'a i32,
    notes: &'a str,
) -> LogEntry {
    use schema::entries;

    let new_entry = NewLogEntry {
        callsign: callsign,
        start_time: start_time,
        end_time: end_time,
        frequency: frequency,
        mode: mode,
        name: name,
        qth: qth,
        state: state,
        country: country,
        grid: grid,
        power: power,
        notes: notes,
    };

    insert_into(entries::table)
        .values(&new_entry)
        .get_result(conn)
        .expect("Save failed")
}
