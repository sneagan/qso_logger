use chrono::prelude::*;
use super::schema::entries;

#[derive(Queryable)]
pub struct LogEntry {
    pub id: i32,
    pub callsign: String,
    pub start_time: NaiveDateTime,
    pub end_time: NaiveDateTime,
    pub frequency: i32,
    pub mode: String,
    pub name: String,
    pub qth: String,
    pub state: String,
    pub country: String,
    pub grid: String,
    pub power: i32,
    pub notes: String,
}

#[derive(Insertable)]
#[table_name="entries"]
pub struct NewLogEntry<'a> {
    pub callsign: &'a str,
    pub start_time: NaiveDateTime,
    pub end_time: NaiveDateTime,
    pub frequency: &'a i32,
    pub mode: &'a str,
    pub name: &'a str,
    pub qth: &'a str,
    pub state: &'a str,
    pub country: &'a str,
    pub grid: &'a str,
    pub power: &'a i32,
    pub notes: &'a str,
}
