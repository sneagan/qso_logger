CREATE TABLE entries (
    id SERIAL PRIMARY KEY,
    callsign VARCHAR NOT NULL,
    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL,
    frequency INT NOT NULL,
    mode VARCHAR NOT NULL,
    name TEXT NOT NULL,
    qth TEXT NOT NULL,
    state TEXT NOT NULL,
    country TEXT NOT NULL,
    grid TEXT NOT NULL,
    power INT NOT NULL,
    notes TEXT NOT NULL
)
